﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtOptimizerUI
{
    public class DebtGroup
    {
        public List<Debt> Debts = new List<Debt>();

        public DebtGroup(List<Debt> debts)
        {
            Debts = new List<Debt>(debts);
        }

        public double GetMonthly()
        {
            return Debts.Select(x => x.MonthlyAmount).Sum();
        }

        public double GetTotal()
        {
            return Debts.Select(x => x.TotalDebt).Sum();
        }

        public string GetIdenture()
        {
            string output = string.Empty;
            foreach (var debt in Debts)
            {
                output += debt.Name;
            }
            
            return Alphabetize(output);
        }

        /// <summary>
        /// Alphabetize the characters in the string.
        /// </summary>
        private string Alphabetize(string s)
        {
            // Convert to char array.
            char[] a = s.ToCharArray();

            // Sort letters.
            Array.Sort(a);

            // Return modified string.
            return new string(a);
        }

        public string Print()
        {
            string output = "Debt Group:\n\n";
            foreach(var debt in Debts)
            {
                output+= debt.Name+"\n$"+debt.MonthlyAmount+" - Payment\n$"+debt.TotalDebt+" - Total Owed\n\n";
            }

            return output;
        }

        public DebtGroup GetCopy()
        {
            List<Debt> debts = new List<Debt>();

            foreach(var debt in Debts)
            {
                debts.Add(debt.GetCopy());
            }

            var newGroup = new DebtGroup(debts);

            return newGroup;
        }
    }
}
