﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtOptimizerUI
{
    public partial class Form1 : Form
    {
        private static List<DebtGroup> Results = new List<DebtGroup>();

        private static List<string> ResultStrings = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Dive(double amountToPayOff, DebtGroup selectedDebt, List<Debt> debts)
        {
            if (!debts.Any()) return; // we're done here.

            foreach (var debt in debts)
            {
                var shrunkDebts = new List<Debt>(debts);
                shrunkDebts.Remove(debt);

                var newDebtGroup = selectedDebt.GetCopy();

                if (!newDebtGroup.Debts.Contains(debt))
                {
                    newDebtGroup.Debts.Add(debt);

                    if (newDebtGroup.GetTotal() <= amountToPayOff)
                    {
                        // no duplicates
                        if (!Results.Any(x => x.GetIdenture() == newDebtGroup.GetIdenture()))
                        {
                            Results.Add(newDebtGroup);
                        }

                        // dive!
                        Dive(amountToPayOff, newDebtGroup, shrunkDebts);
                    }
                }
            }
        }

        private async void OptimizeButton_Click(object sender, EventArgs e)
        {
            List<Debt> allDebts = new List<Debt>();
            Results.Clear();
            ResultStrings.Clear();

            foreach(var item in DebtListBox.Items)
            {
                allDebts.Add((Debt)item);
            }

            if(allDebts.Count == 0)
            {
                MessageBox.Show("No debts found, please enter some.");
                return;
            }

            var amountToPayOff = GetDigits(WindfallTextbox.Text);

            //MessageBox.Show("Alright, I will find the optimal payout given you have $" + amountToPayOff);
            ResultTextBox.Text = "Loading Results...\nThis may take some time depending on the amount of entries so be patient. :)";
            Refresh();

            OptimizeButton.Enabled = false; 

            List<DebtGroup> debtGroups = new List<DebtGroup>();

            await Task.Run(() =>
            {
                Dive(amountToPayOff, new DebtGroup(new List<Debt>() { }), new List<Debt>(allDebts));

                var distinctOptions = Results.GroupBy(x => x.GetIdenture()).Select(x => x.First());

                var orderedByLargestMonthlyPayment = distinctOptions.OrderBy(x => x.GetMonthly());

                for (int i = orderedByLargestMonthlyPayment.Count() - 1; i >= 0; i--)
                {
                    string resultString = string.Empty;
                    resultString += "You should pay off the following...\n";
                    resultString += orderedByLargestMonthlyPayment.ElementAt(i).Print() + "\n";
                    resultString += "Total Cost of Debt Group $" + orderedByLargestMonthlyPayment.ElementAt(i).GetTotal() + "\n";
                    resultString += "Total Saved Per Month $" + orderedByLargestMonthlyPayment.ElementAt(i).GetMonthly() + "\n";
                    resultString += "Press Next Option to see more options...";
                    ResultStrings.Add(resultString);
                }

            });

            if (!ResultStrings.Any()) return;

            ResultTextBox.Text = ResultStrings.First();

            OptimizeButton.Enabled = true;

        }

        private void AddDebtButton_Click(object sender, EventArgs e)
        {
            double monthlyPayment = GetDigits(DebtPaymentTextbox.Text);
            double debtTotal = GetDigits(DebtTotalTextbox.Text);
            string debtName = DebtNameTextbox.Text;
            var newDebt = new Debt()
            {
                Name = debtName,
                MonthlyAmount = monthlyPayment,
                TotalDebt = debtTotal,
            };

            bool canAdd = true;
            foreach(Debt item in DebtListBox.Items)
            {
                if(item.Name == newDebt.Name)
                {
                    canAdd = false;
                }
            }

            if(canAdd)
            {
                DebtListBox.Items.Add(newDebt);
            }
            else
            {
                MessageBox.Show("Your debt needs to have a unique name. Change the name and try again.");
            }

        }

        private double GetDigits(string value)
        {
            var result = "";
            foreach(var charval in value.ToCharArray())
            {
                if(char.IsDigit(charval))
                {
                    result += charval;
                }
            }
            return Convert.ToDouble(result);
        }

        private void DemoButton_Click(object sender, EventArgs e)
        {
            List<Debt> demoDebts = new List<Debt>()
            {
                new Debt {
                    Name = "Loan 1",
                    TotalDebt = 721,
                    MonthlyAmount = 456
                },
                new Debt {
                    Name = "Loan 2",
                    TotalDebt = 603,
                    MonthlyAmount = 93
                },
                new Debt {
                    Name = "Personal Loan",
                    TotalDebt = 2504,
                    MonthlyAmount = 134,
                },
                new Debt {
                    Name = "Boat Loan",
                    TotalDebt = 4825,
                    MonthlyAmount = 160
                },
                new Debt {
                    Name = "Capital One",
                    TotalDebt = 835,
                    MonthlyAmount = 40,
                },
                new Debt {
                    Name = "Credit One",
                    TotalDebt = 973,
                    MonthlyAmount = 50,
                },
                new Debt {
                    Name = "Credit Two",
                    TotalDebt = 674,
                    MonthlyAmount = 40,
                },
                new Debt {
                    Name = "Affirm Shoes",
                    TotalDebt = 542,
                    MonthlyAmount = 91,
                },
                new Debt {
                    Name = "Affirm Phone",
                    TotalDebt = 76,
                    MonthlyAmount = 28,
                },
                new Debt {
                    Name = "Affirm Couch",
                    TotalDebt = 282,
                    MonthlyAmount = 26,
                },
            };

            DebtListBox.Items.Clear();
            foreach(var item in demoDebts)
            {
                DebtListBox.Items.Add(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ResultStrings.Count > 1)
            {
                ResultStrings.RemoveAt(0);
                ResultTextBox.Text = ResultStrings[0];
            }
            else
            {
                MessageBox.Show("Out of options.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ResultStrings.Clear();
            Results.Clear();
            DebtListBox.Items.Clear();
        }
    }
}
