﻿namespace DebtOptimizerUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.DebtListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AddDebtButton = new System.Windows.Forms.Button();
            this.DebtTotalTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DebtPaymentTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.DebtNameTextbox = new System.Windows.Forms.TextBox();
            this.ResultTextBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.OptimizeButton = new System.Windows.Forms.Button();
            this.WindfallTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DemoButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DebtListBox
            // 
            this.DebtListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DebtListBox.DisplayMember = "DisplayName";
            this.DebtListBox.FormattingEnabled = true;
            this.DebtListBox.Location = new System.Drawing.Point(12, 40);
            this.DebtListBox.Name = "DebtListBox";
            this.DebtListBox.Size = new System.Drawing.Size(603, 225);
            this.DebtListBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Debts";
            // 
            // AddDebtButton
            // 
            this.AddDebtButton.Location = new System.Drawing.Point(432, 292);
            this.AddDebtButton.Name = "AddDebtButton";
            this.AddDebtButton.Size = new System.Drawing.Size(183, 23);
            this.AddDebtButton.TabIndex = 2;
            this.AddDebtButton.Text = "Add Debt";
            this.AddDebtButton.UseVisualStyleBackColor = true;
            this.AddDebtButton.Click += new System.EventHandler(this.AddDebtButton_Click);
            // 
            // DebtTotalTextbox
            // 
            this.DebtTotalTextbox.Location = new System.Drawing.Point(292, 295);
            this.DebtTotalTextbox.Name = "DebtTotalTextbox";
            this.DebtTotalTextbox.Size = new System.Drawing.Size(134, 20);
            this.DebtTotalTextbox.TabIndex = 4;
            this.DebtTotalTextbox.Text = "$823";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(292, 279);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Debt Total";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(149, 279);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Debt Monthly Payment";
            // 
            // DebtPaymentTextbox
            // 
            this.DebtPaymentTextbox.Location = new System.Drawing.Point(152, 295);
            this.DebtPaymentTextbox.Name = "DebtPaymentTextbox";
            this.DebtPaymentTextbox.Size = new System.Drawing.Size(134, 20);
            this.DebtPaymentTextbox.TabIndex = 7;
            this.DebtPaymentTextbox.Text = "$150";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 279);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Debt Name";
            // 
            // DebtNameTextbox
            // 
            this.DebtNameTextbox.Location = new System.Drawing.Point(12, 295);
            this.DebtNameTextbox.Name = "DebtNameTextbox";
            this.DebtNameTextbox.Size = new System.Drawing.Size(134, 20);
            this.DebtNameTextbox.TabIndex = 9;
            this.DebtNameTextbox.Text = "Capital One";
            // 
            // ResultTextBox
            // 
            this.ResultTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ResultTextBox.Location = new System.Drawing.Point(12, 400);
            this.ResultTextBox.Name = "ResultTextBox";
            this.ResultTextBox.Size = new System.Drawing.Size(603, 140);
            this.ResultTextBox.TabIndex = 11;
            this.ResultTextBox.Text = "";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 373);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 24);
            this.label5.TabIndex = 12;
            this.label5.Text = "Results";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(12, 552);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(183, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Next Option";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // OptimizeButton
            // 
            this.OptimizeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OptimizeButton.BackColor = System.Drawing.SystemColors.Highlight;
            this.OptimizeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptimizeButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.OptimizeButton.Location = new System.Drawing.Point(413, 546);
            this.OptimizeButton.Name = "OptimizeButton";
            this.OptimizeButton.Size = new System.Drawing.Size(202, 35);
            this.OptimizeButton.TabIndex = 14;
            this.OptimizeButton.Text = "START";
            this.OptimizeButton.UseVisualStyleBackColor = false;
            this.OptimizeButton.Click += new System.EventHandler(this.OptimizeButton_Click);
            // 
            // WindfallTextbox
            // 
            this.WindfallTextbox.Location = new System.Drawing.Point(12, 336);
            this.WindfallTextbox.Name = "WindfallTextbox";
            this.WindfallTextbox.Size = new System.Drawing.Size(134, 20);
            this.WindfallTextbox.TabIndex = 15;
            this.WindfallTextbox.Text = "$2,500";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(148, 336);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 20);
            this.label6.TabIndex = 16;
            this.label6.Text = "<- Your Windfall";
            // 
            // DemoButton
            // 
            this.DemoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DemoButton.Location = new System.Drawing.Point(432, 12);
            this.DemoButton.Name = "DemoButton";
            this.DemoButton.Size = new System.Drawing.Size(183, 23);
            this.DemoButton.TabIndex = 17;
            this.DemoButton.Text = "Demo";
            this.DemoButton.UseVisualStyleBackColor = true;
            this.DemoButton.Click += new System.EventHandler(this.DemoButton_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(243, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(183, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "Reset List";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 588);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.DemoButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.WindfallTextbox);
            this.Controls.Add(this.OptimizeButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ResultTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DebtNameTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DebtPaymentTextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DebtTotalTextbox);
            this.Controls.Add(this.AddDebtButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DebtListBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Debt Optimizer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox DebtListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AddDebtButton;
        private System.Windows.Forms.TextBox DebtTotalTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox DebtPaymentTextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox DebtNameTextbox;
        private System.Windows.Forms.RichTextBox ResultTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button OptimizeButton;
        private System.Windows.Forms.TextBox WindfallTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button DemoButton;
        private System.Windows.Forms.Button button2;
    }
}

