﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DebtOptimizer
{
    public class Debt
    {
        /// <summary>
        /// Gets or sets the name of the debt in question.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets how much is paid per month on this debt.
        /// </summary>
        public double MonthlyAmount { get; set; }

        /// <summary>
        /// Gets or sets how much is owed in total on the debt.
        /// </summary>
        public double TotalDebt { get; set; }

        public Debt GetCopy()
        {
            return new Debt
            {
                Name = Name,
                MonthlyAmount = MonthlyAmount,
                TotalDebt = TotalDebt,
            };
        }
    }
}
