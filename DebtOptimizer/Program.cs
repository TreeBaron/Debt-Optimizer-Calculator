﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtOptimizer
{
    class Program
    {
        private static List<Debt> AllDebts = new List<Debt>()
        {
            /*
            new Debt {
                Name = "Reach Loan",
                TotalDebt = 721,
                MonthlyAmount = 456
            },
            new Debt {
                Name = "Opportun Loan",
                TotalDebt = 603,
                MonthlyAmount = 93
            },
            new Debt {
                Name = "Mariner Loan",
                TotalDebt = 2504,
                MonthlyAmount = 134,
            },*/
            new Debt {
                Name = "Lendmark Loan",
                TotalDebt = 4825,
                MonthlyAmount = 160
            },
            new Debt {
                Name = "Capital One Visa",
                TotalDebt = 835,
                MonthlyAmount = 40,
            },
            /*
            new Debt {
                Name = "Credit One Visa",
                TotalDebt = 973,
                MonthlyAmount = 50,
            },
            */
            /*
            new Debt {
                Name = "Credit One Amex",
                TotalDebt = 674,
                MonthlyAmount = 40,
            },
            */
            new Debt {
                Name = "DreamCloud Affirm",
                TotalDebt = 542,
                MonthlyAmount = 91,
            },
            /*
            new Debt {
                Name = "Amazon Affirm",
                TotalDebt = 76,
                MonthlyAmount = 28,
            },*/
            new Debt {
                Name = "Samsung Affirm",
                TotalDebt = 282,
                MonthlyAmount = 26,
            },
        };

        private static List<DebtGroup> Results = new List<DebtGroup>();

        static void Main(string[] args)
        {
            // verify debts have unique names...
            bool uniqueNames = AllDebts.Select(x => x.Name).Distinct().Count() == AllDebts.Count();

            if(!uniqueNames)
            {
                Console.WriteLine("Your debts do not have unique names assigned to them which is required. Please ensure all names are unique.");
                Console.ReadLine();
                return; // end program
            }

            Console.WriteLine("Total Debt $" + AllDebts.Select(x => x.TotalDebt).Sum());
            Console.WriteLine("Enter Amount you have: ");

            string payoff = Console.ReadLine();
            double amountToPayOff;
            double.TryParse(payoff, out amountToPayOff);

            Console.WriteLine("Alright, I will find the optimal payout given you have $"+amountToPayOff);

            List<DebtGroup> debtGroups = new List<DebtGroup>();

            Dive(amountToPayOff, new DebtGroup(new List<Debt>() { }), new List<Debt>(AllDebts));

            Console.WriteLine("Generated "+Results.Count+" possible combinations!");

            var distinctOptions = Results.GroupBy(x => x.GetIdenture()).Select(x => x.First());

            Console.WriteLine($"Selected distinct combinations. {distinctOptions.Count()} remain.");
            Console.WriteLine("Sorting by largest monthly payment...");

            var orderedByLargestMonthlyPayment = distinctOptions.OrderBy(x => x.GetMonthly());

            
            for (int i = orderedByLargestMonthlyPayment.Count() -1; i >= 0; i--)
            {
                Console.WriteLine("You should pay off the following...\n");
                orderedByLargestMonthlyPayment.ElementAt(i).Print();
                Console.WriteLine("Total Cost of Debt Group $" + orderedByLargestMonthlyPayment.ElementAt(i).GetTotal());
                Console.WriteLine("Total Saved Per Month $" + orderedByLargestMonthlyPayment.ElementAt(i).GetMonthly());

                Console.WriteLine("\nPress Enter to see more options...");

                Console.ReadLine();
                Console.Clear();
            }

            Console.ReadLine();
        }

        private static void Dive(double amountToPayOff, DebtGroup selectedDebt, List<Debt> debts)
        {
            if (!debts.Any()) return; // we're done here.

            foreach (var debt in debts)
            {
                var shrunkDebts = new List<Debt>(debts);
                shrunkDebts.Remove(debt);

                var newDebtGroup = selectedDebt.GetCopy();

                if (!newDebtGroup.Debts.Contains(debt))
                {
                    newDebtGroup.Debts.Add(debt);

                    if (newDebtGroup.GetTotal() <= amountToPayOff)
                    {
                        // no duplicates
                        if (!Results.Any(x => x.GetIdenture() == newDebtGroup.GetIdenture()))
                        {
                            Results.Add(newDebtGroup);
                        }

                        // dive!
                        Dive(amountToPayOff, newDebtGroup, shrunkDebts);
                    }
                }
            }
        }
    }
}
